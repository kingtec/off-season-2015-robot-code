
package org.usfirst.frc.team2169.robot;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Victor;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 * this is ben's version of the code. 
 */
public class Robot extends IterativeRobot {
	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	RobotDrive drive = new RobotDrive(1, 2, 3, 4);
	Joystick rightDriveStick = new Joystick(1);
	Joystick leftDriveStick = new Joystick(2);
	Joystick thirdStick = new Joystick(3);
	Victor lift = new Victor(1);
	Compressor mainCompressor = new Compressor(1);
	Solenoid manipulatorOut = new Solenoid(0);
	Solenoid manipulatorIn = new Solenoid(1);
	Solenoid intakeGrab = new Solenoid (2);
	Solenoid intakeOpen = new Solenoid (3);
	Solenoid bananasOpen = new Solenoid (4);
	Solenoid bananasClose = new Solenoid (5);
	Solenoid leanBack = new Solenoid (6);
	Solenoid leanForward = new Solenoid (7);
	Victor intakes = new Victor(2);

	public void robotInit() {
		mainCompressor.start();
	}

	/**
	 * This function is called periodically during autonomous
	 */
	public void autonomousPeriodic() {

	}

	/**
	 * This function is called periodically during operator control
	 */
	public void teleopPeriodic() {
		drive.tankDrive(leftDriveStick, rightDriveStick, true);
		if (thirdStick.getRawAxis(2) > 20 && thirdStick.getRawAxis(2) > 20) {
			lift.set(thirdStick.getRawAxis(2) / 45);
		}
		if (thirdStick.getRawButton(2)) {
			if (manipulatorOut.equals(true)) {
				manipulatorOut.set(false);
				manipulatorIn.set(true);
				if (manipulatorOut.equals(false)) {
					manipulatorOut.set(true);
					manipulatorIn.set(false);
				}
			}
			//This says that when the second button on the third stick is pressed, and the boolean value is set to true, it changes to false, 
			//and if it is false, it changes to true.
		}
		
		if (thirdStick.getRawButton(1)) {
			if(bananasOpen.equals(true)) {
				bananasOpen.set(false);
				bananasClose.set(true);
				if (bananasClose.equals(true)) {
					bananasClose.set(false);
					bananasOpen.set(true);
				}
			}
		}
		
		if (thirdStick.getRawButton(4)) {
			if(leanBack.equals(true)) {
				leanBack.set(false);
				leanForward.set(true);
				if (leanForward.equals(true)) {
					leanForward.set(false);
					leanBack.set(true);
				}
			}
		}
		
		if (thirdStick.getRawButton(3)) {
			if(intakeGrab.equals(false)) {
				intakeGrab.set(true);
				intakeOpen.set(false);
				if (intakeOpen.equals(false)){
					intakeOpen.set(true);
					intakeGrab.set(false);
				}
			}
		
		if (thirdStick.getRawButton(5)) {
			if (thirdStick.getRawAxis(2) > 0) {
				intakes.set(thirdStick.getRawAxis(2) / 45);
				
			}
			if (thirdStick.getRawAxis(2) < 0) {
				intakes.set(thirdStick.getRawAxis(2) / 45);
			}
		}
		}
		
	}

	/**
	 * This function is called periodically during test mode
	 */
	public void testPeriodic() {

	}

}
